#!/bin/sh

# Create EXAMPLE RabbitUSER1
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER1 $RabbitUSER1 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER1 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER1  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER1' with password '$RabbitUSER1' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER2
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER2 $RabbitUSER2 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER2 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER2  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER2' with password '$RabbitUSER2' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER3
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER3 $RabbitUSER3 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER3 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER3  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER3' with password '$RabbitUSER3' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER4
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER4 $RabbitUSER4 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER4 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER4  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER4' with password '$RabbitUSER4' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER5
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER5 $RabbitUSER5 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER5 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER5  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER5' with password '$RabbitUSER5' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER6
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER6 $RabbitUSER6 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER6 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER6  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER6' with password '$RabbitUSER6' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER7
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER7 $RabbitUSER7 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER7 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER7  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER7' with password '$RabbitUSER7' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER8
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER8 $RabbitUSER8 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER8 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER8  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER8' with password '$RabbitUSER8' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &

# Create EXAMPLE RabbitUSER9
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RabbitUSER9 $RabbitUSER9 2>/dev/null ; \
rabbitmqctl set_user_tags $RabbitUSER9 administrator ; \
rabbitmqctl set_permissions -p / $RabbitUSER9  ".*" ".*" ".*" ; \
echo "*** User '$RabbitUSER9' with password '$RabbitUSER9' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***") &


# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
