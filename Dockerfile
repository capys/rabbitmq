FROM rabbitmq:3.7-management
RUN rabbitmq-plugins enable --offline rabbitmq_mqtt rabbitmq_federation_management rabbitmq_stomp


ENV RABBITMQ_PID_FILE /var/lib/rabbitmq/mnesia/rabbitmq
EXPOSE 15672

ENV RabbitUSER1=RabbitUSER1
ENV RabbitUSER2=
ENV RabbitUSER3=
ENV RabbitUSER4=
ENV RabbitUSER5=
ENV RabbitUSER6=
ENV RabbitUSER7=
ENV RabbitUSER8=
ENV RabbitUSER9=

ADD usercreate.sh /usercreate.sh
RUN chmod +x /usercreate.sh


ADD rabbitmq-isolated.conf /etc/rabbitmq/rabbitmq.config

# Define default command
CMD ["/usercreate.sh"]
