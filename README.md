# Rabbitmq

Initial release: 210222

update 210324:

Added upto 10 users. user names in .env file to be used with docker-compose.

# Using the container

Download or build the container from the repo: https://gitlab.com/capys/rabbitmq/container_registry

`docker pull registry.gitlab.com/capys/rabbitmq:x86` **or** `docker pull registry.gitlab.com/capys/rabbitmq:arm64`

The user names can be edited via the environment variables eg:

`docker run -itd -p 25672:25672 -p 61613:61613 -p 5672:5672 -p 15672:15672 -p 1883:1883 -e RabbitUSER1=ExampleUSER1 -e RabbitUSER2=ExampleUSER2 -e RabbitUSER3=ExampleUSER3 registry.gitlab.com/capys/rabbitmq:x86`

alternatively, if you have docker-compose installed, you can edit the .env

```
RabbitUSER1=USER1example
RabbitUSER2=USER2example
RabbitUSER3=
RabbitUSER4=
.
.
RabbitUSER9=
```

Leave the user blank if not needed.

Then run,

`docker-compose up`


# Customizable

Edit file usercreate.sh for username and passwords added automatically when rabbit starts.

Edit file rabbitmq-isolated.conf to disable remote guest.
